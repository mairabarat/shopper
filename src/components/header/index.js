import React, {useState, useEffect} from 'react';
import {getCategories} from '../../store/actions/categoryActions';
import './index.css';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
function Header(props) {
    const [menu1, setMenu1] = useState(false);
    const [menu2, setMenu2] = useState(false);
    const {categories} = props.categoryReducer;
    const submenu1 = <div className="submenu">
                        <ul>
                            {categories.map (item => (
                                <li key={item.id}>{item.category}</li>
                            ))}
                        </ul>
                    </div>
    const submenu2 = <div className="submenu">
                        <ul>
                            <li>CHECKOUT</li>
                            <li>CART</li>
                            <li><Link to='login'>MY ACCOUNT</Link></li>
                        </ul>
                    </div>
    
    useEffect (() => {
        props.getCategories();
    }, [])
    return(
        <div className="header">
            <div>
                <img src="http://dessign.net/shopper-woocommerce-theme/wp-content/uploads/2015/03/logo1.png"></img>
            </div>
            <nav className="nav-div">
                <ul>
                    <li>HOME</li>
                    <li>ABOUT</li>
                    <li onClick={() => {setMenu1(!menu1); setMenu2(false)}}>SHOP
                        {menu1 ? submenu1 : ''}
                    </li>
                    <li onClick={() => {setMenu2(!menu2); setMenu1(false)}}>MY ACCOUNT
                        {menu2 ? submenu2 : ''}
                    </li>
                    <li>CART</li>
                    <li>BLOG</li>
                    <li>CONTACT</li>
                </ul>
            </nav>
            <div className="search">
                <input placeholder="Search"></input>
                <img src='https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/search-icon.png'></img>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    categoryReducer: state.categoryReducer
}) 

export default connect(mapStateToProps, {getCategories}) (Header);