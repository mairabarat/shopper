import React from 'react';
import './index.css';
import getProducts from '../../store/actions/productActions';
import {Link, withRouter} from 'react-router-dom';
function Card(props) {
    return (
        <div className="card">
            <Link to={'/product/'+props.product.id}><img src={props.product.img}></img></Link>
            <Link to={'/product/'+props.product.id}><p>{props.product.name}</p></Link>
            <p>${props.product.price}.00</p>
        </div>
    )
}

export default withRouter(Card);