import React, { useEffect } from 'react';
import './index.css';
import {Link} from 'react-router-dom';
import {getBasket, deleteBasket} from '../../store/actions/basketAction';
import { connect } from 'react-redux';
function BasketItem(props) {
    const {basket} = props.basketReducer
    let deleteItems = (id) => {
        basket.map(item => {
            if(item.productId == id){
                props.deleteBasket(item.id);
            }
        })
        props.getBasket();
    }
    let basketItemData = props.basketData?.map(item => (
        <tr>
            <td id='td'>
                <button className='delete-btn' onClick={()=> deleteItems(item.id)}>x</button>
                <img src={item.img}/>
            </td>
            <td>{item.name}</td>
            <td>{item.price}</td>
            <td>
                <input className = 'input-amount' type='number' name="amountInput" value={item.amount}></input>
            </td>
            <td>{item.price * item.amount}</td>
        </tr>
    ))

    let sum = [0]
    props.basketData?.map(item => {
        sum.push(item.price * item.amount);
    })

    console.log(sum)
   
    let totalSum = sum.reduce((prev, next) => prev + next)

    return (
        <div className="basket-item">
            <table>
                <tr>
                    <th></th>
                    <th>Product</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                </tr>
                {basketItemData}
                <tr>
                    <td colSpan='2'>
                        <input className='coupon-input' placeholder="Coupon code"/>
                        <button className='coupon'>Apply coupon</button>
                    </td>
                    <td colSpan='3'></td>
                    <td>
                        <button className='coupon update-cart'>Update cart</button>
                    </td>
                </tr>
            </table>

            <div className='total-cart'>
                <h1>Cart totals</h1>
                <table>
                    <tr>
                        <td>Subtotal</td>
                        <td>${totalSum}</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>${totalSum}</td>
                    </tr>
                </table>
                <button>Proceed to checkout</button>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => ({
    basketReducer : state.basketReducer
})

export default connect(mapStateToProps, {getBasket, deleteBasket})(BasketItem);