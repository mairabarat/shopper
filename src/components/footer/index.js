import React from 'react';
import './index.css';
function Footer(){
    return (
        <div className="footer">
            <div className="row">
                <div className="column">
                    <h1>Shopper WooCommerce Theme</h1>
                    <p>Shopper Free WooCommerce eCommerce WordPress Theme perfect for simple online store, with clean and modern grid layout.</p>
                </div>
                <div className="column">
                    <h1>Woocommerce Plugin</h1>
                    <p>Shopper Free WooCommerce eCommerce WordPress Theme perfect for simple online store, with clean and modern grid layout.</p>
                </div>
                <div className="column">
                    <h1>Email Subscribe</h1>
                    <div>
                        <input type="email" placeholder="Enter Your Email ID"></input>
                        <button>GO</button>
                    </div>
                </div>
            </div>
            <div className="footer-icons">
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/facebook-icon.png"></img></a>
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/twitter-icon.png"></img></a>
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/linkedin-icon.png"></img></a>
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/google-plus-icon.png"></img></a>
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/picasa-icon.png"></img></a>
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/pinterest-icon.png"></img></a>
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/youtube-icon.png"></img></a>
                <a><img src="https://dessign.net/shopper-woocommerce-theme/wp-content/themes/ShopperWooThemePremium3/images/vimeo-icon.png"></img></a>
            </div>
            <p>© 2021 All Rights Reserved.</p>
        </div>
    )
}

export default Footer;