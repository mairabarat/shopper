import React , {useEffect} from 'react';
import Header from '../../components/header';
import Card from '../../components/card';
import Footer from '../../components/footer';
import './index.css';
import { connect } from 'react-redux';
import { getProducts } from '../../store/actions/productActions';
import {Link, withRouter} from 'react-router-dom';
function Home(props) {
    useEffect (() => {
        props.getProducts()
    }, [])

    const {products} = props.productReducer;
    let productsRow = products.map(item => (
        <Card product = {item}/>
    )) 
    return (
        <div>
            <Header/>
            <div className="row">
                {productsRow}
            </div>
            <Footer/>
        </div>
    );
}

const mapStateToProps = (state) => ({
    productReducer: state.productReducer
})

export default connect(mapStateToProps, {getProducts}) (Home);