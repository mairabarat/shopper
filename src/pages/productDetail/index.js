import React , {useEffect, useState} from 'react';
import Header from '../../components/header';
import Card from '../../components/card';
import Footer from '../../components/footer';
import './index.css';
import { connect } from 'react-redux';
import { getProduct } from '../../store/actions/productActions';
import {addToBasket} from '../../store/actions/basketAction';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Basket from '../basket';
import {Link, withRouter} from 'react-router-dom'

function ProductDetail(props) {
    useEffect (() => {
        props.getProduct(props.match.params.id)
    }, [])

    const {product} = props.productReducer
    const [amountInput, setAmountInput] = useState(0);
    const [showViewCard, setShowViewCard] = useState(false);
    let addItem = () => {
        let data = {
            productId:props.match.params.id,
            amount: amountInput
        }
        props.addToBasket(data, setShowViewCard);
    }

    const viewCard = <div className="viewCart">
        <p>' {product.name} ' has been added to your cart.</p>
        <Link to = '/basket'><button>View cart</button></Link>
    </div>

    return (
        <div>
            <Header/>
            <div className="row">
            {showViewCard ? viewCard : ''}
                <div className="product-item">
                    <img src= {product.img}></img>
                </div>
                <div className="product-item">
                    <h1>{product.name}</h1>
                    <p className="price-p">${product.price}.00</p>
                    <p>{product.decription}</p>
                    <div className="add-to-card">
                        <input type='number' name="amountInput" onChange={(e) => setAmountInput(e.target.value)}></input>
                        <button onClick={() => addItem()}>Add to cart</button>
                    </div>
                    <p>SKU: Relax599 Categories: Accessories, All Product, New Arrivals</p>
                </div>
                <Tabs className="tab">
                <TabList>
                    <Tab>Description</Tab>
                </TabList>

                <TabPanel>
                    <h2 className="p-desc">{product.decription}</h2>
                </TabPanel>
        
            </Tabs>
            </div>
            <Footer/>
        </div>
    );
}

const mapStateToProps = (state) => ({
    productReducer: state.productReducer
})

export default connect(mapStateToProps, {getProduct, addToBasket}) (withRouter(ProductDetail));