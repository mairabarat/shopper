import React , {useEffect, useState} from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import './index.css';
import { connect } from 'react-redux';
import {Link, withRouter, useHistory} from 'react-router-dom';
import {getUsers} from '../../store/actions/userActions';

function Login(props) {
    let history = useHistory()
    useEffect(() => {
        props.getUsers();
    }, [])

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const {users} = props.userReducer
    const checkUser = () => {
        users.map(item => {
            if(item.username === username && item.password === password){
                history.push('/');
                return
            }
            alert('Неправильный email или пароль')
        })
    }
    return (
        <div>
            <Header/>
            <div className="row">
                <div className='login-form'>
                    <h1>Login</h1>
                    <form>
                        <label>Username or email address</label>
                        <input type="email" value={username} onChange={(e)=> setUsername(e.target.value)}/>
                        <label>Password</label>
                        <input type="password" value={password} onChange={(e)=> setPassword(e.target.value)}/>
                        <button onClick={()=> checkUser()}>Log in</button>
                        <p>Lost your password?</p>
                    </form>
                </div>
                <div className='txt-block'>
                    <h1>Recent Posts</h1>
                    <p>Shopper Woocommerce wordpress theme will help you sell
                    With This Free Shopper Theme we can Sell Product
                    New Product Will Sell If Your Product is Amazing
                    Creative Blog Post To Sell Your Product
                    Hello world!</p>
                    <h1>Recent Posts</h1>
                    <p>Shopper Woocommerce wordpress theme will help you sell
                    With This Free Shopper Theme we can Sell Product
                    New Product Will Sell If Your Product is Amazing
                    Creative Blog Post To Sell Your Product
                    Hello world!</p>
                </div>
            </div>
            <Footer/>
        </div>
    );
}

const mapStateToProps = (state) => ({
    userReducer: state.userReducer
})

export default connect(mapStateToProps, {getUsers}) (withRouter(Login));