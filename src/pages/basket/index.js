import React, {useEffect} from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import './index.css';
import BasketItem from '../../components/basketItem';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {getBasket} from '../../store/actions/basketAction';
import {getProducts} from '../../store/actions/productActions';

function Basket(props){
    useEffect(() => {
        props.getBasket()
        props.getProducts()
    }, []);
    
    const {basket} = props.basketReducer;
    const {products} = props.productReducer;

    let basketData = [];
    basket?.map(item => {
        products?.map(product => {
            if(+item.productId == product.id){
                basketData.push({...product, amount: item.amount, basketId: item.id})
            }
        })
    })
    console.log(JSON.stringify(basketData));
    return(
        <div>
            <Header/>
            <div className="row">
                <div className='items'>
                    <BasketItem basketData={basketData}/>
                </div>
                <div className='txt-block'>
                    <h1>Recent Posts</h1>
                    <p>Shopper Woocommerce wordpress theme will help you sell
                    With This Free Shopper Theme we can Sell Product
                    New Product Will Sell If Your Product is Amazing
                    Creative Blog Post To Sell Your Product
                    Hello world!</p>
                    <h1>Recent Posts</h1>
                    <p>Shopper Woocommerce wordpress theme will help you sell
                    With This Free Shopper Theme we can Sell Product
                    New Product Will Sell If Your Product is Amazing
                    Creative Blog Post To Sell Your Product
                    Hello world!</p>
                </div>
            </div>
            <Footer/>
        </div>
    )
}

const mapStateToProps = (state) => ({
    basketReducer: state.basketReducer,
    productReducer: state.productReducer
})

export default connect(mapStateToProps, {getBasket, getProducts}) (withRouter(Basket));