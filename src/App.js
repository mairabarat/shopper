import logo from './logo.svg';
import './App.css';
import Home from './pages/home';
import store from './store/index';
import {Provider} from 'react-redux';
import {Route, BrowserRouter} from 'react-router-dom';
import ProductDetail from './pages/productDetail/index';
import Basket from './pages/basket';
import Login from './pages/login';
function App() {
  return (
    <Provider store={store}>
      <div>
        <BrowserRouter>
            <Route exact path='/' component={Home}/>
            <Route exact path='/product/:id' component = {ProductDetail}/>
            <Route exact path='/basket' component={Basket} />
            <Route exact path='/login' component={Login} />
        </BrowserRouter>
       
      </div>
    </Provider>
    
  );
}

export default App;