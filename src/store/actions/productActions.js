import {GET_ERRORS, GET_PRODUCTS, GET_PRODUCT} from './types';
import axios from 'axios';

export const getProducts = () => dispatch => {
    axios.get('http://localhost:3000/products').then (
        response => {
            return dispatch ({
                type: GET_PRODUCTS,
                payload: response.data
            })
        }
    ).catch (
        err => {
            return dispatch ({
                type: GET_ERRORS,
                payload: err.response
            })
        }
    )
}


export const getProduct = (id) => dispatch => {
    axios.get('http://localhost:3000/products/' + id).then (
        response => {
            return dispatch ({
                type: GET_PRODUCT,
                payload: response.data
            })
        }
    ).catch (
        err => {
            return dispatch ({
                type: GET_ERRORS,
                payload: err.response
            })
        }
    )
}

