import {GET_USERS, GET_ERRORS} from './types';
import axios from 'axios';

export const getUsers = () => dispatch => {
    axios.get('http://localhost:3000/users').then (
        response => {
            return dispatch ({
                type: GET_USERS,
                payload: response.data
            })
        }
    ).catch (
        err => {
            return dispatch ({
                type: GET_ERRORS,
                payload: err.response
            })
        }
    )
}
 