import productReducer from './productReducer';
import categoryReducer from './categoryReducer';
import basketReducer from './basketReducer';
import userReducer from './userReducer';
import {combineReducers} from 'redux';

export default combineReducers({
    productReducer,
    categoryReducer,
    basketReducer,
    userReducer
})